function add() {
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    let sum = parseInt(number1.value) + parseInt(number2.value);
    let result = document.querySelector('#result');
    result.innerHTML = sum;
}

function subtraction() {
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    let sum = parseInt(number1.value) - parseInt(number2.value);
    let result = document.querySelector('#result');
    result.innerHTML = sum;
}

function divide() {
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    let sum = parseInt(number1.value) / parseInt(number2.value);
    let result = document.querySelector('#result');
    result.innerHTML = sum;
}

function multiply() {
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    let sum = parseInt(number1.value) * parseInt(number2.value);
    let result = document.querySelector('#result');
    result.innerHTML = sum;
}

function clearCalc() {
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    let result = document.querySelector('#result');
    number1.value = "";
    number2.value = "";
    result.innerHTML = "";
}